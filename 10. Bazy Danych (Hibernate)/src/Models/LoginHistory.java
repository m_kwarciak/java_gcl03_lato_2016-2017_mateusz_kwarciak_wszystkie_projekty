package Models;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Mateusz on 27.05.2017.
 */
@Entity
public class LoginHistory {

    @Id
    @GeneratedValue
    private int idLH;

    @OneToOne
    @JoinColumn(name ="userID")
    private Users user;

    private Timestamp timestamp;

    public int getIdLH() { return idLH; }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }
}
