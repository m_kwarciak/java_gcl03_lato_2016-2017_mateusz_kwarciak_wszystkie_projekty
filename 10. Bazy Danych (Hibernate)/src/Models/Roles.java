package Models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Mateusz on 27.05.2017.
 */
@Entity
public class Roles {

    @Id
    @GeneratedValue
    private int idR;

    private String role;


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getIdR() { return idR; }
}
