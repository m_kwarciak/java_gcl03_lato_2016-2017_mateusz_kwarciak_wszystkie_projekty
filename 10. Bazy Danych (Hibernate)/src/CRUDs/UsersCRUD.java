package CRUDs;

import Models.Users;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class UsersCRUD {


    public void create(EntityManager entityManager, Users user)
    {
        entityManager.getTransaction().begin();

        entityManager.persist(user);

        entityManager.getTransaction().commit();
    }

    public List<Users> readAll(EntityManager entityManager)
    {
        List<Users> userList = null;

        TypedQuery<Users> query = entityManager.createQuery("SELECT u from Users u", Users.class);
        userList = query.getResultList();

        return userList;
    }

    public void update(EntityManager entityManager, int id, Users data)
    {
        entityManager.getTransaction().begin();

        Users user = entityManager.find(Users.class, id);

        user.setLogin(data.getLogin());
        user.setPassword(data.getPassword());
        user.setFirst_name(data.getFirst_name());
        user.setLast_name(data.getLast_name());
        user.setRoles(data.getRoles());
        user.setAddress(data.getAddress());
        user.setAge(data.getAge());

        entityManager.getTransaction().commit();
    }

    public void delete(EntityManager entityManager, int id)
    {
        entityManager.getTransaction().begin();

        Users user = entityManager.find(Users.class, id);

        entityManager.remove(user);

        entityManager.getTransaction().commit();
    }

}
