package CRUDs;

import Models.Roles;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class RolesCRUD {

    public void create(EntityManager entityManager, Roles roles)
    {
        entityManager.getTransaction().begin();

        entityManager.persist(roles);

        entityManager.getTransaction().commit();
    }

    public List<Roles> readAll(EntityManager entityManager)
    {
        List<Roles> rolesList = null;

        TypedQuery<Roles> query = entityManager.createQuery("SELECT r from Roles r", Roles.class);
        rolesList = query.getResultList();

        return rolesList;
    }

    public void update(EntityManager entityManager, int id, Roles data)
    {
        entityManager.getTransaction().begin();

        Roles roles = entityManager.find(Roles.class, id);

        roles.setRole(data.getRole());

        entityManager.getTransaction().commit();
    }

    public void delete(EntityManager entityManager, int id)
    {
        entityManager.getTransaction().begin();

        Roles roles = entityManager.find(Roles.class, id);

        entityManager.remove(roles);

        entityManager.getTransaction().commit();
    }

}
