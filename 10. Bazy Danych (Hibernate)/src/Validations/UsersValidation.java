package Validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mateusz on 27.05.2017.
 */
public class UsersValidation {

    public boolean loginValidation(String login)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[A-Z-a-z-0-9]{4,255}");
        Matcher matcher = pattern.matcher(login);
        result = matcher.matches();

        return result;
    }

    public boolean passwordValidation(String password)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[^\\s]{4,255}");
        Matcher matcher = pattern.matcher(password);
        result = matcher.matches();

        return result;
    }

    public boolean nameValidation(String name)  //Dla FirstName, LastName
    {
        boolean result;

        Pattern pattern = Pattern.compile("[A-Z][a-z]{2,255}");
        Matcher matcher = pattern.matcher(name);
        result = matcher.matches();

        return result;
    }

    public boolean ageValidation(String age)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[0-9]{1,3}");
        Matcher matcher = pattern.matcher(age);
        result = matcher.matches();

        return result;
    }

    public boolean addressValidation(String address)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[a-zA-Z0-9 .,]{5,255}");
        Matcher matcher = pattern.matcher(address);
        result = matcher.matches();

        return result;
    }

    public boolean roleValidation(String role)
    {
        boolean result;

        Pattern pattern = Pattern.compile("[a-z]{1,255}");
        Matcher matcher = pattern.matcher(role);
        result = matcher.matches();

        return result;
    }


}
