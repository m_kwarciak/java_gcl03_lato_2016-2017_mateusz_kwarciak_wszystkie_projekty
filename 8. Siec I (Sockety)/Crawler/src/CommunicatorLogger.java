import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Created by Mateusz on 06.05.2017.
 */
public class CommunicatorLogger implements Logger {

    private static String  host = "localhost";
    private Socket socket;
    private String login = "logger";
    private String password = "logger";
    private DataOutputStream outputStream;

    public CommunicatorLogger()
    {
        try
        {
            socket = new Socket(host, 777);
            outputStream = new DataOutputStream(socket.getOutputStream());
            outputStream.writeUTF(login);
            outputStream.writeUTF(password);

        } catch (UnknownHostException e) {} catch (IOException e) {}
    }
    @Override
    public void log(String status, Student student) {
        try {
            outputStream.writeUTF("4" + status + ": "+ student.toString());
            System.out.println("Komunikat wyslany ");
        } catch (IOException e) {}
    }
}
