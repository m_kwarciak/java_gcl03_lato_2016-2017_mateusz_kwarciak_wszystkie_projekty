package SERVER;

import USERS.UserList;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Created by Mateusz on 15.04.2017.
 */
public class Server extends Thread{

    private static int PORT = 777;
    private static boolean access;


        public void run()
    {
        access=true;
        try {
            UserList userList = new UserList();
            ServerSocket serverSocket = new ServerSocket(PORT);

            while (access) {
                Socket socket = serverSocket.accept();
                ClientThread clientThread = new ClientThread(socket);
                userList.addThread(clientThread);
            }
        }catch (IOException e){}
       return;
    }

   public void STOP() throws IOException {
       UserList userList = new UserList();
       ArrayList<ClientThread> list = userList.getListThread();
       for(ClientThread el: list)
       {
           el.exit();
           //userList.removeThread(el);
       }
       access=false;
   }


}
