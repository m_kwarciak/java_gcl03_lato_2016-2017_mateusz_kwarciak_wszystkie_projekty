package USERS;

import java.net.Socket;

/**
 * Created by Mateusz on 22.04.2017.
 */
public class User {
    private String login;
    private String password;
    private int age;
    private String adress;
    private String sex;

    private Socket socket;

    public User() {}

    public User(String login, String password, int age, String adress, String sex)
    {
        this.login =login;
        this.password = password;
        this.age = age;
        this.adress = adress;
        this.sex = sex;

        this.socket = null;
    }

    public String getLogin() {
        return login;
    }
    public String getPassword() {
        return password;
    }
    public int getAge() {
        return age;
    }
    public String getAdress() {
        return adress;
    }
    public String getSex() {
        return sex;
    }
    public Socket getSocket() { return socket; }

    public void setLogin(String login) {
        this.login = login;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public void setAdress(String adress) {
        this.adress = adress;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public void setSocket(Socket socket) { this.socket = socket; }

    public String toString()
    {
        return login + " " + password;
    }
}
