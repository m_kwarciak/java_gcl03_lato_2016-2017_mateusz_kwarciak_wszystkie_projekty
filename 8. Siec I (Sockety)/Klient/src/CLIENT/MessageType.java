package CLIENT;

/**
 * Created by Mateusz on 26.04.2017.
 */
public class MessageType {

    public static final int REGISTER = 1;
    public static final int LOGIN = 2;
    public static final int LOGOUT =3;
    public static final int MESSAGE =4;
    public static final int STATISTICS =5;
    public static final int EXIT = 6;

    public static int decode(String message)
    {
        int result =0;

        if (message.length()==0)
            return 0;
        String msg = message.substring(0,1);
        if (msg.equals("1")) // REGISTER
        {
            return 1;
        } else if(msg.equals("2")) // LOGIN
        {
            return 2;
        }else if (msg.equals("3")) // LOGOUT
        {
            return 3;
        }else if (msg.equals("4")) // MESSAGE
        {
            return 4;
        }else if (msg.equals("5")) // STATISTICS
        {
            return 5;
        }else if (msg.equals("6")) // EXIT
        {
            return 6;
        }


        return result;
    }
}
