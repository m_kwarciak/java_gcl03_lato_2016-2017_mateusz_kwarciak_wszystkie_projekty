package Loggers;

import Program.Student;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mateusz on 03.05.2017.
 */
public class TextLogger implements Logger, Closeable{
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss:ms");
    private final static String PATH = "TEXTLOG\\textLogger.txt";
    private Writer writer;

    @Override
    public void log(String status, Student student) {
        Date date = new Date();

        if (!(new File(PATH).isFile()))
        {
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATH, false),"UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        else
        {
            try {
                writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(PATH, true), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        try {
            writer.write(dateFormat.format(date) + ": " + status + ": " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
            writer.write(System.lineSeparator());
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String textToZIP(String status,Student student) {
        return (status + ": " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
    }


    @Override
    public void close() throws IOException {
        System.out.println("Close");
    }

}
