package Program;

import Loggers.*;

import java.io.IOException;

/**
 * Created by Mateusz on 09.04.2017.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException, IOException
    {
        //CompressedLogger compressedLogger = new CompressedLogger();


        UserOBList userOBList = new UserOBList();
        final Logger[] loggers = new Logger[]
                {
                        new ConsoleLogger(),
                        new GUILogger(),
                        new TextLogger(),
                        new CompressedLogger(),
                        new SerializedLogger(),
                        new BinaryLogger()
                };

        Crawler crawler = new Crawler();
        crawler.sciezka="plik.txt";


        //crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
        //crawler.addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje"));

        //crawler.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
        //crawler.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[1].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[1].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[2].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[2].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[3].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[3].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[4].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[4].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[5].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[5].log("REMOVED",Student));

        try {
            crawler.run();
        }
        catch (CrawlerException e)
        {
            e.pisz();
        }
    }
}
