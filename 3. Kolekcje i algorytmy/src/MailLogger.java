import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Created by Mateusz on 20.03.2017.
 */

public class
MailLogger implements Logger {
    @Override
    public void log(String status, Student student) {
        String dowyslania="puste";
        if(student!=null)
        {
            dowyslania= status + ": " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge();
        }

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("javadziaba@gmail.com","avaJ$555");
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("JavaDziaba@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("joker4050@gmail.com"));
            message.setSubject(status);
            message.setText(dowyslania);

            Transport.send(message);


        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }

    }
}
