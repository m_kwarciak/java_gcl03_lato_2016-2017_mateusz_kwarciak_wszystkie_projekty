import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws InterruptedException, IOException {

		final Logger[] loggers = new Logger[]
				{
						new ConsoleLogger(),
						new MailLogger()
				};


		Crawler crawler = new Crawler();
		crawler.sciezka="plik.txt";
		crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
		crawler.addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje"));

		crawler.addStudentaddedListener((Student)->loggers[0].log("Dodano",Student));
		crawler.addStudentaddedListener((Student)->loggers[1].log("Dodano",Student));


		crawler.addStudentremovedListener((Student)->loggers[0].log("Usunieto",Student));
		crawler.addStudentremovedListener((Student)->loggers[1].log("Usunieto",Student));


		try {
			crawler.run();
		}
		catch (CrawlerException e)
		{
			e.pisz();
		}





	}

}
