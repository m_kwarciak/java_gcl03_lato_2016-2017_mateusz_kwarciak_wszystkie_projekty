package Server;

@FunctionalInterface
public interface IterationListener {
	void handle(int iteration);
}
