package Server;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Crawler {

	public String sciezka = null;



	private List<IterationListener> iterationStartedListeners = new ArrayList<>();



	public void addIterationStartedListener(IterationListener listener)
	{
		iterationStartedListeners.add(listener);
	}

	public void removeIterationStartedListener(IterationListener listener)
	{
		iterationStartedListeners.remove(listener);
	}


	private List<IterationListener> iterationComplitedListeners = new ArrayList<>();

	public void addIterationComplitedListener(IterationListener listener)
	{
		iterationComplitedListeners.add(listener);
	}

	public void removeIterationComplitedListener(IterationListener listener)
	{
		iterationComplitedListeners.remove(listener);
	}

	private List<StudentListener> StudentaddedListeners = new ArrayList<>();


	public void addStudentaddedListener(StudentListener listener)
	{
		StudentaddedListeners.add(listener);
	}

	public void removeStudentaddedListeners(StudentListener listener)
	{
		StudentaddedListeners.remove(listener);
	}

	private List<StudentListener> StudentRemovedListener = new ArrayList<>();


	public void addStudentremovedListener(StudentListener listener)
	{
		StudentRemovedListener.add(listener);
	}
	public void removeStudentremovedListeners(StudentListener listener){
		StudentaddedListeners.remove(listener);
	}


	public void run() throws InterruptedException, IOException, CrawlerException {

		if(sciezka==null)
		{
			throw new CrawlerException("Brak pliku");
		}


		File file = new File(sciezka);

		List<Student> listaStare = new ArrayList<Student>();

		int iteration = 1;
		while(true)
		{
			for(IterationListener el:iterationStartedListeners)
			{
				el.handle(iteration);
			}


			List<Student> usuniete = new ArrayList<Student>();
			List<Student> dodane = new ArrayList<Student>();

			List<Student> listaNowe = StudentsParser.parse(new InputStreamReader(new FileInputStream( file )));
			Collections.sort(listaNowe);


			for (Student stare: listaStare) // Sprawdza czy zostaly usuniete (1)
			{
				boolean flaga1 = false;
				for (Student nowe: listaNowe)
				{

					if (nowe.equals(stare))

					{
						flaga1=true;
					}
				}

				if(flaga1==false) {
					usuniete.add(stare);
				}
			}

			for (Student nowe: listaNowe) //Sprawdza czy zostaly dodane (2)
			{
				boolean flaga2 = false	;
				for (Student stare: listaStare)
				{
					if (nowe.equals(stare))
					{
						flaga2=true;
					}

				}
				if (flaga2==false)
				{


					dodane.add((nowe));
				}

			}

			for (Student dod: dodane)
			{
				for (StudentListener el : StudentaddedListeners) {
					el.handle(dod);
				}
			}

			for (Student usu: usuniete)
			{
				for (StudentListener el : StudentRemovedListener) {
					el.handle(usu);
				}
			}




			Thread.sleep(1000);

			for(IterationListener el:iterationComplitedListeners)
			{
				el.handle(iteration);
			}
			iteration++;

			usuniete.clear();
			dodane.clear();
			listaStare.clear();
			listaStare = listaNowe;
			Collections.sort(listaStare);
		}
	}


}
