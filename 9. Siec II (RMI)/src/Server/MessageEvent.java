package Server;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Mateusz on 14.05.2017.
 */
public interface MessageEvent extends Serializable, Remote
{
    void messageSended(String message) throws RemoteException;
}
