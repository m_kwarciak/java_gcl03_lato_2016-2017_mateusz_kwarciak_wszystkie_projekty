package Client.Program;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class GUIThread extends Application implements Runnable {

    @Override
    public void start(Stage primaryStage) throws Exception{

        Group group = FXMLLoader.load(this.getClass().getResource("/Client/FXML/MainScene.fxml"));
        Scene scene = new Scene(group);

        primaryStage.setTitle("Crawler");
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    @Override
    public void run() {
        launch();
    }
}
