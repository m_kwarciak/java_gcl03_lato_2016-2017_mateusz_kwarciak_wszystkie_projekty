package Program;

import java.io.IOException;

/**
 * Created by Mateusz on 09.04.2017.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException, IOException
    {

        final Logger[] loggers = new Logger[]
                {
                        new ConsoleLogger(),
                        new GUILogger()
                };

        Crawler crawler = new Crawler();
        crawler.sciezka="plik.txt";

        //crawler.addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
        //crawler.addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje"));

        //crawler.addStudentaddedListener((Student)->loggers[0].log("ADDED",Student));
        //crawler.addStudentremovedListener((Student)->loggers[0].log("REMOVED",Student));

        crawler.addStudentaddedListener((Student)->loggers[1].log("ADDED",Student));
        crawler.addStudentremovedListener((Student)->loggers[1].log("REMOVED",Student));

        try {
            crawler.run();
        }
        catch (CrawlerException e)
        {
            e.pisz();
        }
    }
}
