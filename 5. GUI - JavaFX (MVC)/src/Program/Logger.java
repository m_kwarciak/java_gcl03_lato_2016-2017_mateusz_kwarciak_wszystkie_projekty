package Program;

/**
 * Created by Mateusz on 20.03.2017.
 */
public interface Logger {
    void log(String status, Student student);
}
