import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;


/**
 * Created by Mateusz on 25.03.2017.
 */
public class CustomMenuBar extends javafx.scene.control.MenuBar {

    public static javafx.scene.control.MenuBar makeMenuBar() {

        javafx.scene.control.MenuBar menuBar = new javafx.scene.control.MenuBar();
        Menu programMenu = new Menu("Program");
        Menu aboutMenu = new Menu();
        Label aboutMenuLabel = new Label("About");
        MenuItem closeMenuItem = new Menu("Close   Ctr+C");

        closeMenuItem.setOnAction(actionEvent -> {
            Platform.exit();
            System.exit(0);
        });
        closeMenuItem.setMnemonicParsing(true);
        closeMenuItem.setAccelerator(new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN));

        aboutMenuLabel.setOnMouseClicked(event -> {
            Alert alert = new Alert(Alert.AlertType.NONE);
            alert.setTitle("About");
            alert.getDialogPane().getButtonTypes().add(ButtonType.OK);
            alert.setContentText("Autor: Mateusz Kwarciak");
            alert.show();

        });

        aboutMenu.setGraphic(aboutMenuLabel);

        programMenu.getItems().add(closeMenuItem);
        menuBar.getMenus().addAll(programMenu, aboutMenu);


        return menuBar;
    }
}
