
/**
 * Created by Mateusz on 26.03.2017.
 */
public class GUILogger implements Logger {
    @Override
    public void log(String status, Student student) {

        StudentProperty sp = new StudentProperty(student.getMark(), student.getFirstName(), student.getLastName(), student.getAge());



        if("ADDED".equals(status)) {

            ObList.addToObList(sp, status);
        } else if ("REMOVED".equals(status)){
            ObList.removeFromObList(sp, status);
        }

    }
}
