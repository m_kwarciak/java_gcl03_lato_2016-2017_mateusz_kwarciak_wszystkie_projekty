import javafx.scene.chart.BarChart;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;


/**
 * Created by Mateusz on 25.03.2017.
 */
public class CustomTabPane extends AnchorPane {
    public static TabPane makeTabPane()
    {
        TabPane tabPane = new TabPane();

/////////////////////////////////////////////////////////////TableView
        Tab students = new Tab("Students");

        TableView<StudentProperty> tableView = CustomTableView.makeTableView();
        students.setContent(tableView);

/////////////////////////////////////////////////////////////TableView
        Tab logi = new Tab("Log");

        TableView<Log> tableViewLog = CustomTableVievLog.makeTableViewLog();
        logi.setContent(tableViewLog);


/////////////////////////////////////////////////////////////BarChart
        Tab histogram = new Tab("Histogram");

        BarChart barChart = CustomBarChart.makeBarChart();
        histogram.setContent(barChart);

/////////////////////////////////////////////////////////////ADD TO TABPANE
        tabPane.getTabs().addAll(students, logi, histogram);
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);



        return  tabPane;
    }

}
