import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

/**
 * Created by Mateusz on 23.04.2017.
 */
public class ParalleLogger implements Logger, Runnable {

    private  final Logger[] loggers;
    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private BlockingQueue<StatusObject> logList = new ArrayBlockingQueue<StatusObject>(100);
    private boolean action = true;

    public ParalleLogger()
    {
       loggers = new Logger[]
                {
                        new ConsoleLogger(),
                };
    }


    @Override
    public void log(String status, Student student) {

        if(student!=null) {
            StatusObject tmp = new StatusObject(status, student);
            logList.add(tmp);
        }
    }

    @Override
    public void run() {
        while(action)
        {
            StatusObject tmp;
            try {
                tmp=logList.take();
                if(tmp!=null)
                {
                    loggers[0].log(tmp.getStatus(), tmp.getStudent());
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void postCancel()
    {
        action = false;
    }
}
