/**
 * Created by Mateusz on 23.04.2017.
 */
public class StatusObject {
    private String status;
    private Student student;

    public StatusObject()
    {
    }

    public StatusObject(String status, Student student)
    {
        this.status = status;
        this.student = student;
    }

    public String getStatus() {
        return status;
    }

    public Student getStudent() {
        return student;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
