/**
 * Created by Mateusz on 23.04.2017.
 */
public class Monitor implements Runnable {

        private int howManyThread;
        private String[] pathList;
        private Thread[] threads;
        private Crawler[] crawlers;

        public Monitor(int howManyThread, String[] pathList)
        {
            this.howManyThread = howManyThread;
            this.pathList = pathList;
            this.threads = new Thread[howManyThread];
            this.crawlers = new Crawler[howManyThread];

        }

        public final CustomEvent<Student> studentAddedEvent = new CustomEvent<>();
        public final CustomEvent<Student> studentRemovedEvent = new CustomEvent<>();


    @Override
    public void run() {
        for (int i=0; i<howManyThread; i++)
        {
            try {
                if (pathList[i].isEmpty()) {
                    throw new MonitorException("ERROR: Path is empty");

                }
            }catch (MonitorException e)
            {
                e.write();
                System.exit(1);
            }
            crawlers[i] = new Crawler();
            crawlers[i].sciezka=pathList[i];


            crawlers[i].addIterationStartedListener((iteration)->System.out.println("Rozpoczeto iteracje "+iteration));
            crawlers[i].addIterationComplitedListener(iteration->System.out.println("Zakonczono iteracje"));

            final Crawler tmp =crawlers[i];
            crawlers[i].addStudentaddedListener((Student)-> studentAddedEvent.fire(tmp, Student));
            crawlers[i].addStudentremovedListener((Student)-> studentRemovedEvent.fire(tmp, Student));


            threads[i] = new Thread(crawlers[i]);
            threads[i].start();
            System.out.println("Uruchomiono watek nr: "+(i+1));
        }
        cancel();
    }

    public void cancel()
    {
        try {
            Thread.sleep(30000);
        for (int i=0; i<howManyThread; i++)
            {
                crawlers[i].postCancel();
                threads[i].join();
                System.out.println("Zakonczono watek nr: "+ (i+1));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
