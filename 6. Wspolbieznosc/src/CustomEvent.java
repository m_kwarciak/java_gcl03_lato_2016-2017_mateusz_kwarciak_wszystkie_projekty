import java.util.ArrayList;
import java.util.List;

public class CustomEvent<T>
{
    private List<ICustomListener<T>> listeners = new ArrayList<>();

    public void fire( Object source, T object )
    {
        for( ICustomListener<T> el : this.listeners )
            el.handle( source, object );
    }

    public void add( ICustomListener<T> listener )
    {
        this.listeners.add( listener );
    }

    public void remove( ICustomListener<T> listener )
    {
        this.listeners.remove( listener );
    }
}
