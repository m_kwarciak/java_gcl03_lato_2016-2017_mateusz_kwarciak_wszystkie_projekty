import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Crawler implements Runnable{

	public String sciezka = null;
	private boolean action = true;

	public Crawler() {}

	private List<IterationListener> iterationStartedListeners = new ArrayList<>();
	public void addIterationStartedListener(IterationListener listener)
	{
		iterationStartedListeners.add(listener);
	}
	public void removeIterationStartedListener(IterationListener listener)
	{
		iterationStartedListeners.remove(listener);
	}

	private List<IterationListener> iterationComplitedListeners = new ArrayList<>();
	public void addIterationComplitedListener(IterationListener listener)
	{
		iterationComplitedListeners.add(listener);
	}
	public void removeIterationComplitedListener(IterationListener listener) {iterationComplitedListeners.remove(listener);}

	private List<StudentListener> StudentaddedListeners = new ArrayList<>();
	public void addStudentaddedListener(StudentListener listener)
	{
		StudentaddedListeners.add(listener);
	}
	public void removeStudentaddedListeners(StudentListener listener)
	{
		StudentaddedListeners.remove(listener);
	}

	private List<StudentListener> StudentRemovedListener = new ArrayList<>();
	public void addStudentremovedListener(StudentListener listener){
		StudentRemovedListener.add(listener);
	}
	public void removeStudentremovedListeners(StudentListener listener){
		StudentaddedListeners.remove(listener);
	}



	public void run() {

		try {
			if(sciezka==null)
			{
				throw new CrawlerException("Brak pliku");
			}
		}catch (CrawlerException e)
		{
			e.pisz();
		}



		File file = new File(sciezka);
		List<Student> previousStudentList = new ArrayList<Student>();

		int iteration = 1;
		while(action)
		{
			for(IterationListener el:iterationStartedListeners)
			{
				el.handle(iteration);
			}

			List<Student> addStudent = new ArrayList<Student>();
			List<Student> removeStudent = new ArrayList<Student>();

			List<Student> studentList = null;
			try {
				studentList = StudentsParser.parse(new InputStreamReader(new FileInputStream( file )));
			} catch (IOException e) {
				e.printStackTrace();
			}
			Collections.sort(studentList);


			for (Student stare: previousStudentList) // Sprawdza czy dodano nowe osoby (1)
			{
				boolean flaga1 = false;
				for (Student nowe: studentList)
				{

					if (nowe.equals(stare))

					{
						flaga1=true;
					}
				}

				if(flaga1==false) {
					addStudent.add(stare);
				}
			}

			for (Student nowe: studentList) //Sprawdza czy usunieto nowe osoby (2)
			{
				boolean flaga2 = false	;
				for (Student stare: previousStudentList)
				{
					if (nowe.equals(stare))
					{
						flaga2=true;
					}

				}
				if (flaga2==false)
				{
					removeStudent.add((nowe));
				}

			}

			for (Student dod: removeStudent)
			{
				for (StudentListener el : StudentaddedListeners) {
					el.handle(dod);
				}
			}

			for (Student usu: addStudent)
			{
				for (StudentListener el : StudentRemovedListener) {
					el.handle(usu);
				}
			}


			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			for(IterationListener el:iterationComplitedListeners)
			{
				el.handle(iteration);
			}
			iteration++;

			addStudent.clear();
			removeStudent.clear();
			previousStudentList.clear();
			previousStudentList = studentList;
			Collections.sort(previousStudentList);
		}
	}

	public void postCancel()
	{
		action = false;
	}

}
