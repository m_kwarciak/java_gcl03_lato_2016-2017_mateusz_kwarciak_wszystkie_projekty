import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mateusz on 20.03.2017.
 */
public class ConsoleLogger implements Logger {

    private final static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

    @Override
    public void log(String status, Student student) {
            Date date = new Date();
            System.out.println(dateFormat.format(date) + ": " + status + ": " + student.getMark() + " " + student.getFirstName() + " " + student.getLastName() + " " + student.getAge());
    }
}
