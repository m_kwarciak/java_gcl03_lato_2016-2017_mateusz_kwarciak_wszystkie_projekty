import java.util.List;

@FunctionalInterface
public interface StudentListener {
	void handle(Student student);
}
